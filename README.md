
### What is this Finlibre? ###
Finlibre is an effort at providng a comprehensive

open source analytics solution to the Banking and Insurance sectors.

It is part of RISE Groups's effort in Data Anlytics and extends our

architecture work into fully engineered vertical solutions.


Features

- Support for Canonical Banking and Insurance Schema 

- Staging and Analytics Server

- Data Integration Server

- Datamarts for Analytics

- Visualization

- Reporting

- AI/ML based decision engines




### Contribution guidelines ###
All contrubutors should assign copyright to IIT-Madras.


### Contact Person ###

Contact G S Madhusudan (gs.madhusudan@cse.iitm.ac.in)

### Funding ###

The project is funded by CSR grants from City Union Bank.

### License ###
Finlibre is an open source software pacakage released under the AGPL V3.0

license. See license.txt for details.Please note that as per AGPL V3.0,

even hosting of Finlibre is covered under the license terms.