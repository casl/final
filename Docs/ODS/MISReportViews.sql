-- Report Views
--CUSTOMERS WITHOUT THRESHOLD LIMIT
CREATE VIEW vw_cwotl AS SELECT cvdd.key_1 CUSTOMERNO, cvaa.name1||' '||cvaa.mid_name||' '||cvaa.name2 NAME FROM CUSVDD cvdd JOIN cbs_cusm JOIN CUSVAA cvaa on cvaa.cust_no=substring(cvdd.key_1,4,19) where cvdd.thre_lim =0;

--VIP DETAILS WITH RELATIONSHIP
CREATE VIEW vw_vdwr AS SELECT cusm.home_branch_no HOMEBRANCH, cusm.cust_acct_no CUSTOMERNO, cvaa.name1||' '||cvaa.mid_name||' '||cvaa.name2 NAME, cvaa.add1 ADDRESS1,cvaa.add2 ADDRESS2, cvaa.add3 ADDRESS3, cvaa.add4 ADDRESS4, cvaa.postcode, cusm.ex_attribute_7 MOBILENO,cumi.email_add1 EMAILID, FROM cusm JOIN CUSVAA cvaa on cvaa.cust_no=cusm.cust_acct_no JOIN cumi on cumi.cust_no= cusm.cust_acct_no JOIN telm on telm.WHERE cusm.vip_code=1;

--Birthday CUSTOMERS
CREATE VIEW vw_birthday AS SELECT cusm.ex_attribute_7 MOBILENO, cusm.cust_acct_no CUSTOMERNO, cvaa.name1||' '||cvaa.mid_name||' '||cvaa.name2 NAME FROM cusm JOIN CUSVAA cvaa on cvaa.cust_no=cusm.cust_acct_no ;
-- need details on getting the birth date as it is pointing to cusvaa which represents account number
--VIP BIRTHDAY CUSTOMERS
--to be completed

--CUSTOMER WITHOUT EMAIL ID
CREATE VIEW vw_cwoemail AS SELECT cusm.home_branch_no HOMEBRANCH, cusm.cust_acct_no CUSTOMERNO, cvaa.name1||' '||cvaa.mid_name||' '||cvaa.name2 NAME, cusm.customer_type, cusm.ex_attribute_7 MOBILENO, cusm.vip_code,cusm.tier_cust_type from cusm JOIN CUSVAA cvaa on cvaa.cust_no=cusm.cust_acct_no JOIN cumi on cumi.cust_no= cusm.cust_acct_no WHERE LEN(trim(cumi.email_add1)) = 0;

--CUSTOMER WITHOUT FATHER/MOTHER NAME
CREATE VIEW vw_cwofmname AS SELECT cusm.home_branch_no HOMEBRANCH, cusm.cust_acct_no CUSTOMERNO, cvaa.name1||' '||cvaa.mid_name||' '||cvaa.name2 NAME, cusm.customer_type, cusm.ex_attribute_7 MOBILENO, cusm.vip_code,cusm.tier_cust_type, cumi.email_add1 EMAILID from cusm JOIN CUSVAA cvaa on cvaa.cust_no=cusm.cust_acct_no JOIN cumi on cumi.cust_no= cusm.cust_acct_no WHERE LEN(trim(cvaa.father_name))=0 AND LEN(trim(cvaa.mother_name))=0;

--CUSTOMER WITHOUT MOBILE NO
CREATE VIEW vw_cwomobile AS SELECT cusm.home_branch_no HOMEBRANCH, cusm.cust_acct_no CUSTOMERNO, cvaa.name1||' '||cvaa.mid_name||' '||cvaa.name2 NAME, cusm.customer_type,cusm.vip_code,cusm.tier_cust_type, cumi.email_add1 EMAILID from cusm JOIN CUSVAA cvaa on cvaa.cust_no=cusm.cust_acct_no JOIN cumi on cumi.cust_no= cusm.cust_acct_no WHERE LEN(trim(cusm.ex_attribute_7))=0;

--CUSTOMER WITHOUT PAN NO
CREATE VIEW vw_cwopan AS SELECT cusm.home_branch_no HOMEBRANCH, cusm.cust_acct_no CUSTOMERNO, cvaa.name1||' '||cvaa.mid_name||' '||cvaa.name2 NAME, cusm.customer_type,cusm.vip_code,cusm.tier_cust_type, cusm.ex_attribute_7 MOBILENO, cumi.email_add1 EMAILID from cusm JOIN CUSVAA cvaa on cvaa.cust_no=cusm.cust_acct_no JOIN cumi on cumi.cust_no= cusm.cust_acct_no WHERE LEN(trim(cusm.cust_tax_pan))=0;



