### We use following Open Source Software in FinAL stack: ###
* GO - 1.8
* Java - 1.7
* Postgres - 9.6
* Metabase - v0.26.1
* Drools - 7.3



### Source Code & Installation setup were downloaded from the following URLs: ###

* GO - 1.8:
Installation Setup: https://redirector.gvt1.com/edgedl/go/go1.8.5.linux-amd64.tar.gz
Source Code: https://redirector.gvt1.com/edgedl/go/go1.8.5.src.tar.gz -> go1.8.5.src.tar.gz


* Java - 1.7:
Installation Setup: http://download.oracle.com/otn/java/jdk/7u80-b15/jdk-7u80-linux-i586.tar.gz


* Metabase - v0.26.1 :
Installation Setup: http://downloads.metabase.com/v0.26.2/metabase.jar
Source Code: https://github.com/metabase/metabase/releases/tag/v0.26.2 -> metabase-0.26.2.tar.gz


* Postgres - 9.6 :
Installation Setup: https://get.enterprisedb.com/postgresql/postgresql-9.6.5-1-linux-x64-binaries.tar.gz?_ga=2.78165456.597652679.1510143034-1550443176.1510143034
Source Code: https://www.postgresql.org/ftp/source/v9.6.2/ -> postgresql-9.6.2.tar.gz


* Drools - 7.3 :
Installation Setup: https://download.jboss.org/drools/release/7.4.1.Final/drools-distribution-7.4.1.Final.zip
Source Code: https://github.com/kiegroup/drools/releases/tag/7.3.1.t013 -> drools-7.3.1.t013.tar.gz

