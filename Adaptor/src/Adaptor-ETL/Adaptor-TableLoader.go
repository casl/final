/**
 * This GO code will move the data from CSV/DB to Final-Project's Database, Postgres.
 * A JSON is used to map-&-copy all the required tables.
 * JSON will have the column level mapping details.
 * 
 * In this Postgres is used as Final's database. So the fastest way of data copy technique of PG's "COPY" command is used.
 * COPY command with source and target are formed with the mapping JSON.
 * 
 * All tables are copied first to a temporary table, which starts with "intermediate_" (or) "csv_" prefix.
 * Then all the data are moved to core table, with/without JOINs, casting, formatting or other techniques.
 * 
 * 
 * Abbrevations:
 * "BS-N" is "\N"
 * 
 * 
 * Installation:
 * - Extract pq-master.zip under $GOROOT/src/github.com/lib/pq
 *     If requires create the directories `mkdir -p $GOROOT/src/github.com/lib/pq`
 * 
 * 
 * Copyright (C) <2017> IIT-Madras
 * 
 * License:
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 **/

package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"
	_ "github.com/lib/pq"
)

// our Database's Functions, Variables, Constants
// as the Postgres is used for Data-Analytic, PG functions and variables are given below.
const today_12am = "CURRENT_DATE"
const current_datetime = "now()"

/* Structure of the JSON to be formed [ */
// DB connection properties. This can be for both Source & Final's (Target) databases.
type DataBaseConfiguration struct {
	DB_UserName string `json:"db_user_name"`
	DB_Password string `json:"db_password"`
	DB_Name string `json:"db_name"`
	DB_HostName string `json:"db_hostname"`
	DB_Port string `json:"db_port"`
}

// If data is given in CSV format then, following CSV-File parameters are required.
type CSVMode struct {
	FilePath string `json:"filePath"`
	Delimiter string `json:"delimiter"`
	HasHeader bool `json:"has_header"`
	NullString string `json:"null_value"`
	IncrementalCondition string `json:"incremental_condition"`
}

// If data is given directly from a DB then, following DB details are required.
type DBMode struct {
	TableName string `json:"table_name"`
	QueryToExport string `json:"query_to_export"`
	IncrementalCondition string `json:"incremental_condition"`
}

// Source (CSV/DB) & Final-DB's table column mapping.
// Any specific conversion/casting's formula can be given in "Import_Query"
type ColumnMapping struct {
	To string `json:"to"`
	From string `json:"from"`
}

// Table's porting details like source type CSV/DB-direct-read.
// And Column-Mapping for each and every column in the table.
type ImportTable struct {
	Disabled bool `json:"disabled"`
	Intermediate_TableName string `json:"intermediate_tableName"`
	Intermediate_TableColumns string `json:"intermediate_tableColumns"`
	Truncate_Intermediate_Table string `json:"truncate_intermediate_table"`
	Core_TableName string `json:"core_tableName"`
	Truncate_Core_Table string `json:"truncate_core_table"`
	CSV_Mode CSVMode `json:"csv_mode"`
	Is_CSV_Mode bool `json:"is_csv_mode"`
	DB_Mode DBMode `json:"db_mode"`
	Is_DB_Mode bool `json:"is_db_mode"`
	Column_Mapping []ColumnMapping `json:"column_mapping"`
	Post_Operations []string `json:"post_operations"`
}

// This is the master struct, which is parent for source(if available) and final-db connection parameters;
// And tables to be ported.
type Configuration struct {
	Adaptor_PG_Bin_Path	string `json:"adaptor_pg_bin_path"`
	SourceDBConfig DataBaseConfiguration `json:"source"`
	TargetDBConfig DataBaseConfiguration `json:"target"`
	ImportTables []ImportTable `json:"import"`
}
/* Structure of the JSON to be formed ] */


/* Load the mapping configuration json-file.
 * Load it into a srtuct object.
 */
func loadConfig(configFile string) []Configuration {
	file, e := ioutil.ReadFile(configFile)
	if e != nil {
		log.Fatal(e)
		os.Exit(1)
	}
	
	confJson := make([]Configuration, 1)
	e = json.Unmarshal(file, &confJson)
	if e != nil {
		log.Println("JSON parsing error:")
		log.Fatal(e)
		os.Exit(1)
	}
	
	return confJson
}

/* Copy the data from a Source database into our FinAL's database.
 * If the Source Database is Postgres then, 
 * 		Use COPY command and export CSV file from Source DB.
 * 		Import the CSV file into our FinAL DB (Target DB).
 */
func runCopyToCommand(sourceDBConn *sql.DB, tableName, csv_filePath, csv_delimiter, csv_nullString string, csv_hasHeader bool) {
	csvDelimiter := ""
	if len(csv_delimiter) > 0 {
		csvDelimiter = "DELIMITER E'"+csv_delimiter+"'"
	}
	
	csvHasHeader := ""
	if csv_hasHeader {
		csvHasHeader = "HEADER"
	} else {
		csvHasHeader = ""
	}
	
	csvNullString := "";
	if len(csv_nullString) > 0 {
		if strings.Compare(csv_nullString, "BS-N") == 0 {
			csvNullString = "NULL E'\\N'"
		} else if strings.Compare(csv_nullString, "BLANK") == 0 {
			csvNullString = "NULL ''"
		} else {
			csvNullString = "NULL '"+csv_nullString+"'"
		}
	}
	
	copyQuery := "COPY "+tableName+" TO '"+csv_filePath+"' CSV "+csvDelimiter+" "+csvHasHeader+" "+csvNullString+";";
	
	log.Println("Executing.. ", copyQuery);
	
	_, err := sourceDBConn.Exec(copyQuery)
	if err != nil {
		log.Println("Error Query: ", copyQuery);
		log.Fatal(err)
	}
}

/* Format the PSQL's COPY command with CSV import options
 * Available options are: delimiter, header and NULL-value-notation
 */
func runCopyFromCommand(targetDBConn *sql.DB, tableName, columns, csv_filePath, csv_delimiter, csv_nullString string, csv_hasHeader bool) {
	csvDelimiter := ""
	if len(csv_delimiter) > 0 {
		csvDelimiter = "DELIMITER E'"+csv_delimiter+"'"
	}
	
	csvHasHeader := ""
	if csv_hasHeader {
		csvHasHeader = "HEADER"
	} else {
		csvHasHeader = ""
	}
	
	csvNullString := "";
	if len(csv_nullString) > 0 {
		if strings.Compare(csv_nullString, "BS-N") == 0 {
			csvNullString = "NULL E'\\N'"
		} else if strings.Compare(csv_nullString, "BLANK") == 0 {
			csvNullString = "NULL ''"
		} else {
			csvNullString = "NULL '"+csv_nullString+"'"
		}
	}
	
	copyQuery := "COPY "+tableName+columns+" FROM '"+csv_filePath+"' CSV "+csvDelimiter+" "+csvHasHeader+" "+csvNullString+";";
	
	log.Println("Executing.. ", copyQuery);
	
	_, err := targetDBConn.Exec(copyQuery)
	if err != nil {
		log.Println("Error Query: ", copyQuery);
		log.Fatal(err)
	}
}

/* Move the data from temp table, which starts with extension "intermediate_" (or) "csv_", into actual core table.
 * Before starting the operation, validate the mapping JSON data for blank.
 */
func importTempToCoreTable(targetDBConn *sql.DB, table ImportTable) {
	FromColumns := ""
	ToColumns := ""
	insertQuery := ""
	
	// move the data from "intermediate_*/csv_*" table into "core" table, only if "Core_TableName" present
	if len(table.Intermediate_TableName) > 0 && len(table.Core_TableName) > 0 {
		for _, column := range table.Column_Mapping {
			// log.Printf("Column: %q %q %q\n", column.From, column.To, column.Import_Query)
			
			// Validate column-name presence
			if len(column.From) == 0 {
				panic("Column-name is blank in \"From\" field for the table \""+table.Core_TableName+"\".")
			}
			if len(column.To) == 0 {
				panic("Column-name is blank in \"To\" field for the table \""+table.Core_TableName+"\".")
			}
			
			FromColumns = FromColumns + column.From + ","
			ToColumns = ToColumns + column.To + ","
		}
		
		FromColumns = strings.Trim(FromColumns, ",")
		ToColumns = strings.Trim(ToColumns, ",")
		
		insertQuery = "INSERT INTO " + table.Core_TableName + "(" + ToColumns + ")" + " SELECT " + FromColumns + " FROM " + table.Intermediate_TableName
		
		if len(table.CSV_Mode.IncrementalCondition) > 0 {
			insertQuery = insertQuery+" WHERE " + table.CSV_Mode.IncrementalCondition
		}
		
		insertQuery = strings.Replace(insertQuery, "today_12am", today_12am, -1)
		
		log.Println("Executing.. ", insertQuery);
		
		_, err := targetDBConn.Exec(insertQuery)
		if err != nil {
			log.Println("Error Query: ", insertQuery);
			log.Fatal(err)
		}
	}
}


func executeQuery(targetDBConn *sql.DB, query string) {
	
	log.Println("Executing.. ", query);
	
	_, err := targetDBConn.Exec(query)
	if err != nil {
		log.Println("Error Query: ", query);
		log.Fatal(err)
	}
}

/*
 * The starting function of the Import operations.
 * Creates the DB-Connection of Final's Postgres-DB and passes it through-out the program.
 * Loops through each table's details given in JSON. And move it with Postgres's COPY command.
 */
func main() {
	query := "";
	Target_TableName := ""
	Target_Columns := ""
	sourceTableQuery := ""
	
	inputConfigFile := os.Args[1]
	
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)
	
	configurations := loadConfig( inputConfigFile )
	
	pwd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	
	log.Printf("input JSON: %+v\n\n", configurations)
	//log.Printf("input JSON: %#v\n", configurations)
	
	TargetDBConfig := configurations[0].TargetDBConfig
	SourceDBConfig := configurations[0].SourceDBConfig
	
	// Initialize Source as CSV variables
	sourceAsCSV_Delimiter := ";";
	sourceAsCSV_NullString := "BS-N";
	sourceAsCSV_HasHeader := true;
	sourceAsCSV_File := "";
	sourceAsCSV_FilePath := pwd + string(os.PathSeparator) + "source_as_csv";
	currentTime := time.Now()
	sourceAsCSV_File_Suffix := currentTime.Format("20060102_150405")+".csv"
	
	// Initialize Target DB-Connection
	targetDBInfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", TargetDBConfig.DB_UserName, TargetDBConfig.DB_Password, TargetDBConfig.DB_Name)
	targetDBConn, err := sql.Open("postgres", targetDBInfo)
	if err != nil {
		log.Fatal(err)
	}
	defer targetDBConn.Close()
	
	// declare Source-DB-Connection variable, which will be used later, only if required.
	var sourceDBConn *sql.DB;
	
	// Initialize Source DB-Connection, if available
	if len(SourceDBConfig.DB_UserName) > 0 && len(SourceDBConfig.DB_Name) > 0 {
		sourceDBInfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", SourceDBConfig.DB_UserName, SourceDBConfig.DB_Password, SourceDBConfig.DB_Name)
		sourceDBConn, err := sql.Open("postgres", sourceDBInfo)
		if err != nil {
			log.Fatal(err)
		}
		defer sourceDBConn.Close()
	}
	
	// Loop through each table-map JSON
	for _, table := range configurations[0].ImportTables {
		
		if table.Disabled {
			log.Printf("Skipping Table: %v", table)
			continue;
		}
		
		// Confirm the Target-Table
		if( len(table.Intermediate_TableName) > 0 ) {
			Target_TableName = table.Intermediate_TableName
			Target_Columns = table.Intermediate_TableColumns
		} else {
			Target_TableName = table.Core_TableName
			Target_Columns = ""
		}
		
		// Validations ::
		// Validate presence of both CSV & Source DB
		if len(table.CSV_Mode.FilePath) > 0 && len(table.DB_Mode.TableName) > 0 {
			panic("Can't have both CSV & DB mode; to load Table: "+Target_TableName)
		}
		
		// Initialize transfer mode : CSV/DB
		if len(table.CSV_Mode.FilePath) > 0 {
			table.Is_CSV_Mode = true;
			table.Is_DB_Mode = false;
			
			if strings.Index(table.CSV_Mode.FilePath, ".") == 0 {
				table.CSV_Mode.FilePath = pwd + string(os.PathSeparator) + table.CSV_Mode.FilePath;
			}
		} else if len(table.DB_Mode.TableName) > 0 {
			table.Is_CSV_Mode = false;
			table.Is_DB_Mode = true;
		}
		
		// Clear the "intermediate_/csv_" table, only if asked
		if len(table.Intermediate_TableName) > 0 {
			query = "";
			if strings.Compare(table.Truncate_Intermediate_Table, "TRUNCATE") == 0 {
				query = "TRUNCATE TABLE "+table.Intermediate_TableName;
			} else if strings.Compare(table.Truncate_Intermediate_Table, "TRUNCATE_RESET") == 0 {
				query = "TRUNCATE TABLE "+table.Intermediate_TableName+" RESTART IDENTITY";
			}
			if len(query) > 0 {
				log.Println("Executing..", query)
				_, err := targetDBConn.Exec( query )
				if err != nil {
					log.Fatal(err)
				}
			}
		}
		
		// Clear the "core_" table data, only if asked
		if len(table.Core_TableName) > 0 {
			query = "";
			if strings.Compare(table.Truncate_Core_Table, "TRUNCATE") == 0 {
				query = "TRUNCATE TABLE "+table.Core_TableName;
			} else if strings.Compare(table.Truncate_Core_Table, "TRUNCATE_RESET") == 0 {
				query = "TRUNCATE TABLE "+table.Core_TableName+" RESTART IDENTITY";
			}
			if len(query) > 0 {
				log.Println("Executing..", query)
				_, err := targetDBConn.Exec( query )
				if err != nil {
					log.Fatal(err)
				}
			}
		}
		
		// Load CSV into "Intermediate_/csv_/Core_" process table
		if table.Is_CSV_Mode {
			log.Println("Importing :: from CSV file: ", table.CSV_Mode.FilePath, "; into Table: ", Target_TableName,Target_Columns);
			
			runCopyFromCommand(targetDBConn, Target_TableName, Target_Columns, table.CSV_Mode.FilePath, table.CSV_Mode.Delimiter, table.CSV_Mode.NullString, table.CSV_Mode.HasHeader);
			
		} else 
		// Load Source.Table into "Intermediate_/csv_/Core_" process table
		if table.Is_DB_Mode {
			log.Println("Importing :: from Source-DB: ", SourceDBConfig.DB_Name, "; into Table: ", Target_TableName, Target_Columns)
			
			if len(table.DB_Mode.QueryToExport) > 0 && len(table.DB_Mode.IncrementalCondition) > 0 {
				panic("Can't have both Query-To-Export & Incremental-Condition; to load Table: "+Target_TableName+Target_Columns)
				
			} else if len(table.DB_Mode.QueryToExport) > 0 {
				sourceTableQuery = table.DB_Mode.QueryToExport;
			} else if len(table.DB_Mode.TableName) > 0 {
				sourceTableQuery = "SELECT * FROM "+table.DB_Mode.TableName;
				
				if len(table.DB_Mode.IncrementalCondition) > 0 {
					sourceTableQuery = sourceTableQuery+" WHERE "+table.DB_Mode.IncrementalCondition;
				}
			}
			
			sourceAsCSV_File = sourceAsCSV_FilePath + string(os.PathSeparator) + Target_TableName + "_" + sourceAsCSV_File_Suffix;
			
			// Copy the data from Source-DB into a CSV file.
			if len(SourceDBConfig.DB_UserName) > 0 && len(SourceDBConfig.DB_Name) > 0 {
				log.Println("Importing :: Source table as CSV file: ", sourceTableQuery, "; to : ", sourceAsCSV_File);
				runCopyToCommand(sourceDBConn, sourceTableQuery, sourceAsCSV_File, sourceAsCSV_Delimiter, sourceAsCSV_NullString, sourceAsCSV_HasHeader);
			}
			
			// As the CSV is available, in the location "sourceAsCSV_File", Now copy the CSV into Target table.
			runCopyFromCommand(targetDBConn, Target_TableName, Target_Columns, sourceAsCSV_File, sourceAsCSV_Delimiter, sourceAsCSV_NullString, sourceAsCSV_HasHeader);
		}
		
		// Move the date from "intermediate_/csv_" table to core table
		// This will be done, ONLY-IF Intermediate_TableName AND Core_TableName are present
		importTempToCoreTable(targetDBConn, table)
		
		
		// TODO: Execute the Move-to-Core Array
		
		// Move the data from multiple "intermediate_/csv_" tables into particular core table.
		for _, post_operation := range table.Post_Operations {
			if len(post_operation) > 0 {
				executeQuery(targetDBConn, post_operation)
			}
		}
		
		log.Println("Completed.\n")
	}
}
