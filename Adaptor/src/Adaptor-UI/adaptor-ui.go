/**
 * 
 * Abbrevations:
 * "BS-N" is "\N"
 * 
 * 
**/

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"io/ioutil"
)

/* Structure of the JSON to be formed [ */
// DB connection properties. This can be for both Source & Final's (Target) databases.
type DataBaseConfiguration struct {
	DB_UserName string `json:"db_user_name"`
	DB_Password string `json:"db_password"`
	DB_Name string `json:"db_name"`
	DB_HostName string `json:"db_hostname"`
	DB_Port string `json:"db_port"`
}

// If data is given in CSV format then, following CSV-File parameters are required.
type CSVMode struct {
	FilePath string `json:"filePath"`
	Delimiter string `json:"delimiter"`
	HasHeader bool `json:"has_header"`
	NullString string `json:"null_value"`
	IncrementalCondition string `json:"incremental_condition"`
}

// If data is given directly from a DB then, following DB details are required.
type DBMode struct {
	TableName string `json:"table_name"`
	IncrementalCondition string `json:"incremental_condition"`
}

// Source (CSV/DB) & Final-DB's table column mapping.
// Any specific conversion/casting's formula can be given in "Import_Query"
type ColumnMapping struct {
	To string `json:"to"`
	From string `json:"from"`
}

// Table's porting details like source type CSV/DB-direct-read.
// And Column-Mapping for each and every column in the table.
type ImportTable struct {
	Disabled bool `json:"disabled"`
	Intermediate_TableName string `json:"intermediate_tableName"`
	Intermediate_TableColumns string `json:"intermediate_tableColumns"`
	Truncate_Intermediate_Table string `json:"truncate_intermediate_table"`
	Core_TableName string `json:"core_tableName"`
	Truncate_Core_Table string `json:"truncate_core_table"`
	CSV_Mode CSVMode `json:"csv_mode"`
	Is_CSV_Mode bool `json:"is_csv_mode"`
	DB_Mode DBMode `json:"db_mode"`
	Is_DB_Mode bool `json:"is_db_mode"`
	Column_Mapping []ColumnMapping `json:"column_mapping"`
	Post_Operations []string `json:"post_operations"`
}

// This is the master struct, which is parent for source(if available) and final-db connection parameters;
// And tables to be ported.
type Configuration struct {
	Adaptor_PG_Bin_Path	string `json:"adaptor_pg_bin_path"`
	SourceDBConfig DataBaseConfiguration `json:"source"`
	TargetDBConfig DataBaseConfiguration `json:"target"`
	ImportTables []ImportTable `json:"import"`
}
/* Structure of the JSON to be formed ] */


func SaveForm(w http.ResponseWriter, r * http.Request) {
	
	r.ParseForm()
	
	configFileName := strings.Join( r.PostForm["fileName"], ",")
	log.Println("saving configFileName: ",configFileName);
	
	adaptor_pg_bin_path := r.PostForm["adaptor_pg_bin_path"]
	target := r.PostForm["Target"]
	source := r.PostForm["Source"]
	
	//log.Printf("Import: %+v", r.Form["Import"])
	STR := make([]string,len(r.Form["Import"]))
	
	for i:=0; i < len(r.PostForm["Import"]); i++ {
		STR[i] = r.PostForm["Import"][i]
		//log.Printf("Import: %+v", STR[i])
	}
	
	Adaptor_pg_bin_path := strings.Join(adaptor_pg_bin_path, ",")
	Target := strings.Join(target, ",")
	Source := strings.Join(source, ",")
	Import := strings.Join(STR, ",")
	
	//log.Printf("join-all: %+v", "[{\"adaptor_pg_bin_path\":\""+ Adaptor_pg_bin_path +"\",\"target\":"+ Target +",\"source\":"+ Source +",\"import\":["+ Import +"]}]")
	
	confJson := make([]Configuration, 1)
	e := json.Unmarshal([]byte("[]"), &confJson)	// dummy code to avoid exception
	if len(source) > 0 {
		e = json.Unmarshal( []byte("[{\"adaptor_pg_bin_path\":\""+ Adaptor_pg_bin_path +"\",\"target\":"+ Target +",\"source\":"+ Source +",\"import\":["+ Import +"]}]"), &confJson)
	} else {
		e = json.Unmarshal( []byte("[{\"adaptor_pg_bin_path\":\""+ Adaptor_pg_bin_path +"\",\"target\":"+ Target +",\"import\":["+ Import +"]}]"), &confJson)
	}
	if e != nil {
		log.Printf("JSON parsing error: %v\n", e)
		log.Printf("JSON: %v\n",confJson)
	}
	//log.Printf("confJson: %+v", confJson);
	
	json, err := json.MarshalIndent(confJson, "", "\t")
	if err != nil {
		log.Fatal(err)
	}
	//log.Printf("json: %+v", json);
	
	file, _ := os.OpenFile(configFileName,os.O_CREATE|os.O_TRUNC|os.O_WRONLY,0644)
	defer file.Close()
	//log.Println(string(json));
	_, _ = file.WriteString( string(json) )
}

func ReadForm(w http.ResponseWriter, r * http.Request) {
	
	r.ParseForm()
	configFileName := strings.Join( r.PostForm["fileName"], ",")
	log.Println("reading configFileName: ",configFileName);
	
	byteData, err := ioutil.ReadFile(configFileName)
	if err != nil {
		log.Println(err)
		return
	}
	
	stringData := string(byteData)
	
	//stringData = strings.Replace(stringData, " true,", " \"true\",", -1)
	//stringData = strings.Replace(stringData, " false,", " \"false\",", -1)
	
	w.Write([]byte(stringData))
}

func main() {
	
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)
	
	port := flag.Int("port", 3010, "port to serve on")
	dir := flag.String("directory", "web/", "directory of web files")
	flag.Parse()
	
	fs := http.Dir(*dir)
	fileHandler := http.FileServer(fs)
	http.Handle("/", fileHandler)
	http.HandleFunc("/saveForm", SaveForm)
	http.HandleFunc("/readForm", ReadForm)
	log.Printf("Running on port %d\n", *port)
	
	addr := fmt.Sprintf("0.0.0.0:%d", *port)
	
	err := http.ListenAndServe(addr, nil)
	log.Println(err.Error())
}
