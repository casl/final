var app = angular.module('FormApp', []);

app.directive('autogrow', function () {
	return {
		restrict: 'A',
		link: function postLink(scope, element, attrs) {
			// hidding the scroll of textarea
			element.css('overflow', 'hidden');
			
			var update = function(){
				
				element.css("height", "auto");
				
				var height = element[0].scrollHeight;
				
				if(height > 0){
					element.css("height", height + "px");
				}
			};
			
			scope.$watch(attrs.ngModel, function(){
				update();
			});
			
			attrs.$set("ngTrim", "false");
		}
	};
});


app.controller('FormController', function($scope,$http,$httpParamSerializer){
	/*$scope.configFiles = 
	[
		{
			"nickName": "Masters",
			"fileName": "../_config/config_TableMap_CUB-v3.5_20180308_master.json"
		},{
			"nickName": "Transactions",
			"fileName": "../_config/config_TableMap_CUB-v3.5_20180308_transaction.json"
		},{
			"nickName": "Test",
			"fileName": "../_config/config_TableMap_CUB-v3.5_20180308_test.json"
		}
	];*/
	$http({
		method: 'POST',
		url: '../config.json',
		headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
	}).then(function(response) {
		$scope.configFiles = (response.data);
	});
	
	$scope.showLoading = false;
	
	$scope.regexpSemiColon = new RegExp(";", "g");
	$scope.regexpJoinPO = new RegExp("#@#","g")
	
	$scope.target = {
		'name': '',
		'password' : '',
		'db' : '',
		'host' : '',
		'port': ''
	};
	
	$scope.SourceDB = {
		'name': '',
		'password' : '',
		'db' : '',
		'host' : '',
		'port': ''
	};
	
	$scope.fieldGroupTemplate = {
		'minimized': false,
		'disabled': false,
		'intermediate_tableName': '',
		'intermediate_tableColumns': '',
		'truncate_intermediate_table': 'TRUNCATE_RESET',
		'core_tableName': '',
		'truncate_core_table': 'TRUNCATE_RESET',
		'file_path': '',
		'delimiter': ',',
		'has_header': false,
		'null_string': 'BS-N',
		'column_mapping' : [
			{'to': '', 'from': ''}
		],
		'post_operations': ['']
	};
	
	
	$scope.fieldGroups = [angular.copy($scope.fieldGroupTemplate)];
	
	$scope.addFieldGroup = function() {
		$scope.fieldGroups.push(angular.copy($scope.fieldGroupTemplate));
	}
	$scope.minimizeFieldGroup = function(index) {
		$scope.fieldGroups[index].minimized = ! $scope.fieldGroups[index].minimized;
	}
	
	$scope.inactivateFieldGroup = function(index) {
		$scope.fieldGroups[index].disabled = ! $scope.fieldGroups[index].disabled;
	}
	
	$scope.addMapping = function(index){
		$scope.fieldGroups[index].column_mapping.push({"to":"","from":""});
	}
	$scope.removeMapping = function(fieldGroupIndex, colMapIndex){
		/* console.info("index: "+fieldGroupIndex);
		console.info("index1: "+colMapIndex); */
		$scope.fieldGroups[fieldGroupIndex].column_mapping.splice(colMapIndex, 1);
	}
	
	$scope.submitForm = function() {
		
		var mydata = { /* adaptor_pg_bin_path: ($scope.adaptor_pg_bin_path).replace(/\\/g, "\\\\"), */ Target: $scope.target, Import: $scope.fieldGroups, Source: $scope.SourceDB, fileName: $scope.selectedConfiguration.fileName};
		
		if($scope.target.name==''){
			return false;
		}
		if($scope.target.password==''){
			return false;
		}
		if($scope.target.db==''){
			return false;
		}
		if($scope.target.host==''){
			return false;
		}
		if($scope.target.port==''){
			return false;
		}
		
		for(var i=0;i<$scope.fieldGroups.length;i++){
			var allPostOperations = $scope.fieldGroups[i].post_operations.join("#@#").replace($scope.regexpJoinPO,"").trim()
			
			if( allPostOperations.length == 0 ) {
				
				if($scope.fieldGroups[i].intermediate_tableName==''){
					return false;
				}
				if($scope.fieldGroups[i].core_tableName==''){
					return false;
				}
				if($scope.fieldGroups[i].filePath==''){
					return false;
				}
				if($scope.fieldGroups[i].delimiter==''){
					return false;
				}
				if($scope.fieldGroups[i].has_header==''){
					return false;
				}
				if($scope.fieldGroups[i].null_value==''){
					return false;
				}
				if($scope.fieldGroups[i].column_mapping.to==''){
					return false;
				}
				if($scope.fieldGroups[i].column_mapping.from==''){
					return false;
				}
			}
		}
		
		$http({
			method: 'POST',
			url: '../saveForm',
			data: $httpParamSerializer(mydata).replace($scope.regexpSemiColon,"%3B"),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
		}).then(function(data) {
			alert("success");
		});
	};
	
	$scope.fillForm = function(){
		$scope.showLoading = true;
		
		$http({
			method: 'POST',
			url: '../readForm',
			data: $httpParamSerializer({
				fileName: $scope.selectedConfiguration.fileName
			}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
		}).then(function(response) {
			$scope.adaptor_pg_bin_path = (response.data[0].adaptor_pg_bin_path);
			$scope.target = (response.data[0].target);
			$scope.SourceDB = (response.data[0].source);
			$scope.fieldGroups = [];
			
			for(var j=0;j<(response.data[0]).import.length;j++){
				$scope.fieldGroups[j] = ( (response.data[0]).import[j]);
			}
			
			$scope.showLoading = false;
		});
	};
});
