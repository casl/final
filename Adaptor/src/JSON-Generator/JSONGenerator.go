/**
 * Generate Config_TableMap-JSON through FinAL-TableStructureProposed-vX.Y.xls file's first two columns.
 * 
 * Abbrevations:
 * "BS-N" is "\N"
 * 
 * 
 * Compile:
 * H:\Final\BitBucket_Repo\Adaptor\src\JSON-Generator>go build JSONGenerator.go
 * 
 * 
 * Execute: 
 *     > JSONGenerator.exe TableName_ColumnName.csv Template_Config.json Output_Config_TableMap.json
 * Eg. > JSONGenerator.exe "../_csv/FinAL-TableStructureProposed-v3.5_20180308_master.csv" "../_config/config_TableMap_JSONGenerator_Template.json" "../_config/config_TableMap_CUB-v3.5_20180308_master.json"
 * 
**/

package main

import (
	"bufio"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"strings"
//	"time"
)

// If data is given in CSV format then, following CSV-File parameters are required.
type CSVMode struct {
	FilePath string `json:"filePath"`
	Delimiter string `json:"delimiter"`
	HasHeader bool `json:"has_header"`
	NullString string `json:"null_value"`
	IncrementalCondition string `json:"incremental_condition"`
}

// If data is given directly from a DB then, following DB details are required.
type DBMode struct {
	TableName string `json:"table_name"`
	IncrementalCondition string `json:"incremental_condition"`
}

// Source (CSV/DB) & Final-DB's table column mapping.
// Any specific conversion/casting's formula can be given in "Import_Query"
type ColumnMapping struct {
	To string `json:"to"`
	From string `json:"from"`
}

// Table's porting details like source type CSV/DB-direct-read.
// And Column-Mapping for each and every column in the table.
type ImportTable struct {
	Disabled bool `json:"disabled"`
	Intermediate_TableName string `json:"intermediate_tableName"`
	Intermediate_TableColumns string `json:"intermediate_tableColumns"`
	Truncate_Intermediate_Table string `json:"truncate_intermediate_table"`
	Core_TableName string `json:"core_tableName"`
	Truncate_Core_Table string `json:"truncate_core_table"`
	CSV_Mode CSVMode `json:"csv_mode"`
	Is_CSV_Mode bool `json:"is_csv_mode"`
	DB_Mode DBMode `json:"db_mode"`
	Is_DB_Mode bool `json:"is_db_mode"`
	Column_Mapping []ColumnMapping `json:"column_mapping"`
	Post_Operations []string `json:"post_operations"`
}

func main() {
	
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)
	
	// Check for args
	if len(os.Args) != 4 {
		panic("Arguments are missing. Please refer the documents.")
	}
	
	// Get i/o filepath details from command-line arguments
	inputCSV := os.Args[1];
	templateConfigFile := os.Args[2];
	outputConfigFile := os.Args[3];
	
	// Declare all variables
	columnName := ""
	tableName := ""
	csv_mode := CSVMode{};
	db_mode := DBMode{};
	columnMappings := make([]ColumnMapping, 0)
	importTable := ImportTable{}
	importTables := make([]ImportTable, 0)
	
	// Open CSV file, which has two columns TableName & ColumnName
	file, err := os.Open( inputCSV )
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	defer file.Close()
	
	// Read the file one-by-one line
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		
		// Empty Line: process the read Table-Column details
		if strings.TrimSpace(line) == "" {
			if len(columnMappings) != 0 {
				// process 
				log.Println("processing...\n", tableName);
				
				// Initialize default values
				csv_mode = CSVMode{FilePath: "/CSV_FILE_PATH/csv_file.csv", Delimiter: ",", HasHeader: false, NullString: "BS-N", IncrementalCondition: ""}
				db_mode = DBMode{}
				importTable = ImportTable{Disabled: false, Intermediate_TableName: "csv_"+tableName, intermediate_tableColumns: "", Truncate_Intermediate_Table: "TRUNCATE_RESET", Core_TableName: tableName, Truncate_Core_Table: "TRUNCATE_RESET", CSV_Mode: csv_mode, Is_CSV_Mode: true, DB_Mode: db_mode, Is_DB_Mode: false, Column_Mapping: columnMappings, Post_Operations: [""]};
				
				// Append into root structure
				importTables = append(importTables, importTable)
				
				// Clear variables for next iteration (next line)
				columnName = ""
				tableName = ""
				columnMappings = make([]ColumnMapping, 0);
			}
		} else 
		// First-Column empty: Found a column
		if strings.Index(line, "\t") == 0 {
			columnName = strings.TrimSpace(line);
			log.Println(" col: ", columnName);
			columnMappings = append(columnMappings, ColumnMapping{From: columnName, To: columnName});
		} else 
		// First-Column exists: Found a table
		if strings.Index( strings.TrimRight(line, "\t"), "\t") == -1 && strings.TrimSpace(line) != "" {
			tableName = strings.TrimSpace(line);
			log.Println("Table: ", tableName);
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	
	// Parse the String into JSON, with a "double-tab" in prefix & a "tab" as indentation character. 
	json, err := json.MarshalIndent(importTables, "\t\t", "\t")
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	
	// Read and keep the template content
	template, err := ioutil.ReadFile( templateConfigFile )
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	
	// Replace "import" JSON in template
	strConfigJSON := strings.Replace(string(template), "#@GENERATED_JSON@#", string(json), -1)
	
	log.Printf("Output JSON: %+v\n\n", strConfigJSON)
	
	// Write the JSON into a file
	err = ioutil.WriteFile(outputConfigFile,([]byte(strConfigJSON)),0600);
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	
	// Show the destination file details
	log.Printf("JSON written in file: ", outputConfigFile)
}
