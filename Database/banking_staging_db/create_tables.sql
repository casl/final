CREATE TABLE account_type  (
id serial ,
type int ,
int_cat int ,
description varchar ,
created_on timestamptz  ,
modified_on timestamptz  ,
primary key(id)  ,
unique(type,int_cat)  );

CREATE TABLE branch_master  (
branch_code int ,
branch_name varchar ,
branch_state int ,
branch_scale varchar ,
branch_address varchar ,
pin_code varchar ,
email varchar ,
telephone varchar ,
rank int,
created_on timestamptz  ,
modified_on timestamptz  ,
unique(branch_code)  );

CREATE TABLE account_master  (
customer_id bigint ,
account_no bigint ,
account_branch int ,
acct_type int ,
int_cat int ,
created_on timestamptz  ,
modified_on timestamptz  ,
unique(account_no)  );
);

CREATE TABLE atm_master  (
id serial ,
branch_code int ,
mobile_1 varchar ,
mobile_2 varchar ,
manager_mobile varchar ,
mobile_3 varchar ,
atm_id varchar ,
atm_loc varchar ,
atm_type varchar ,
atm_site varchar ,
atm_city varchar ,
atm_pincode varchar ,
atm_address varchar ,
population varchar ,
custodian varchar ,
latitude varchar ,
longitude varchar ,
created_on timestamptz  ,
modified_on timestamptz  ,
primary key(id)  ,
unique(atm_id)  );

CREATE TABLE branch_region_mapping  (
branch_code int ,
brname varchar ,
region varchar ,
created_on timestamptz  ,
modified_on timestamptz  ,
unique(branch_code,region)  );

CREATE TABLE state_master  (
state_code int ,
description varchar ,
country_id int ,
created_on timestamptz  ,
modified_on timestamptz  ,
primary key(state_code)  );

CREATE TABLE transaction_details  (
acct_no bigint ,
tran_date timestamptz  ,
jrnl_no bigint ,
tran_type int ,
tran_code bigint ,
tlr_no bigint ,
branch_code int ,
tran_amt numeric(18,3) ,
bal numeric(18,3) ,
remarks varchar ,
created_on timestamptz  ,
modified_on timestamptz  ,
unique(acct_no,tran_date,jrnl_no)  );

CREATE TABLE transaction_code  (
tran_code bigint ,
description varchar ,
created_on timestamptz  ,
modified_on timestamptz  ,
unique(tran_code)  );

CREATE TABLE customer(
id serial NOT NULL ,
customer_id bigint  ,
customer_name varchar  ,
home_branch_code int  ,
address varchar  ,
pan varchar  ,
mobile_no varchar  ,
email varchar  ,
state_code int  ,
post_code varchar  ,
unique(customer_id)   ,
primary key(id)   );

CREATE TABLE postcode  (
id serial NOT NULL ,
country_code varchar  ,
postal_code varchar  ,
place_name varchar  ,
state varchar  ,
county varchar  ,
community varchar  ,
lat numeric(10,6)  ,
long numeric(10,6)  ,
accuracy int  ,
state_code int  ,
rank int,
created_on timestamptz   ,
modified_on timestamptz   ,
primary key(id)   );
