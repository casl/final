/* Reports as Views.
 * This can be used in MetaBase as Questions.
 */

CREATE OR REPLACE VIEW public.vwecom AS
 SELECT td.tran_date,
    br.branch_name,
	sm.description as state_name ,
    c.customer_name,
    td.acct_no,
    td.tran_code,
    tc.description,
    abs(td.tran_amt) 
   FROM transaction_details td
     JOIN account_master am ON am.account_no = td.acct_no
     JOIN customer c ON c.customer_id = am.customer_id
     JOIN transaction_code tc ON tc.tran_code = td.tran_code
     JOIN branch_master br ON br.branch_code = td.branch_code
	 JOIN state_master sm on sm.state_code=br.branch_state
  WHERE td.tran_amt < '-20000'::integer::numeric AND td.tran_code = 4061
  ORDER BY td.tran_date, td.tran_amt desc

ALTER TABLE public.vwecom
    OWNER TO postgres;


CREATE OR REPLACE VIEW public.vwpos AS
 SELECT td.tran_date,
    br.branch_name,
	sm.description AS State_Name,
    c.customer_name,
    td.acct_no,
    td.tran_code,
    tc.description,
    abs(td.tran_amt)
   FROM transaction_details td
     JOIN account_master am ON am.account_no = td.acct_no
     JOIN customer c ON c.customer_id = am.customer_id
     JOIN transaction_code tc ON tc.tran_code = td.tran_code
     JOIN branch_master br ON br.branch_code = td.branch_code
	 JOIN state_master sm on sm.state_code=br.branch_state
  WHERE td.tran_amt < '-20000'::integer::numeric AND td.tran_code = 4066
  ORDER BY td.tran_date, br.branch_name 

  
CREATE OR REPLACE VIEW public.vwtrf AS
 SELECT td.tran_date,
    br.branch_name,
	sm.description AS State_Name,
    c.customer_name,
    td.acct_no,
    td.tran_code,
    tc.description,
    abs(td.tran_amt) AS abs
   FROM transaction_details td
     JOIN account_master am ON am.account_no = td.acct_no
     JOIN customer c ON c.customer_id = am.customer_id
     JOIN transaction_code tc ON tc.tran_code = td.tran_code
     JOIN branch_master br ON br.branch_code = td.branch_code
	 JOIN state_master sm on sm.state_code=br.branch_state
  WHERE (td.tran_amt <= '-100000'::integer::numeric or td.tran_amt >='100000'::integer::numeric) AND (td.tran_code = 1045 OR td.tran_Code=1055)
  ORDER BY td.tran_date, br.branch_name;
  
ALTER TABLE public.vwtrf
    OWNER TO postgres;

create index idx_td_tran_code on transaction_details(tran_code);
create index id_td_tran_amt on transaction_details(tran_amt);

cub1234
venkatraman.k@cityunionbank.com
manager@cub.com


3Cgo3BF2HNHqKW

