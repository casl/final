/*
 * Table required for Health-Insurance data copy.
 */

CREATE TABLE city (
	id SERIAL PRIMARY KEY,
	city_name VARCHAR(50),
	state VARCHAR(50)
);

CREATE TABLE city_cnt (
	city_id INT,
	state VARCHAR(100),
	cnt INT
);

CREATE TABLE claim_details (
	id BIGSERIAL PRIMARY KEY,
	doc TIMESTAMP WITHOUT TIME ZONE,
	client_id BIGINT,
	claim_amt numeric (10, 0),
	hospital_id INT,
	disease_id INT,
	policy_id BIGINT,
	accepted_amt numeric (10, 0),
	remarks VARCHAR(200),
	is_rejected bit
);

CREATE TABLE client_fnl (
	id BIGSERIAL PRIMARY KEY,
	first_name VARCHAR(100),
	last_name VARCHAR(100),
	dob date,
	gender VARCHAR(8),
	city_id INT
);

CREATE TABLE data_dist_plan (
	date TIMESTAMP WITHOUT TIME ZONE,
	tot_claim_per_day INT,
	I_claim INT,
	II_claim INT,
	III_claim INT
);

CREATE TABLE data_dist_policy (
	date DATE,
	weeknum SMALLINT,
	total_policy_cnt INT,
	ren_cnt INT,
	new_pol_cnt INT
);

CREATE TABLE disease (
	id INT,
	disease_name VARCHAR(200),
	max_claim_amt numeric (10, 0)
);

CREATE TABLE hospital_fnl (
	id BIGSERIAL PRIMARY KEY,
	hospital_name VARCHAR(200),
	city_id INT,
	city VARCHAR(100),
	state VARCHAR(100),
	category VARCHAR(50),
	hos_rank INT
);

CREATE TABLE policy_master (
	id BIGSERIAL PRIMARY KEY,
	client_id BIGINT,
	policy_name VARCHAR(25),
	policy_amt numeric (10, 0),
	dop TIMESTAMP WITHOUT TIME ZONE,
	doe TIMESTAMP WITHOUT TIME ZONE,
	prm_amt numeric (10, 0)
);

CREATE TABLE policy_renewal_details (
	id SERIAL PRIMARY KEY,
	policy_id BIGINT,
	renewal_date TIMESTAMP WITHOUT TIME ZONE,
	old_expiry_date TIMESTAMP WITHOUT TIME ZONE,
	new_expiry_date TIMESTAMP WITHOUT TIME ZONE
);

CREATE TABLE policy_slab (
	id INT,
	policy_name VARCHAR(50),
	policy_amt numeric (10, 0),
	prm_amt numeric (7, 0),
	age_from INT,
	age_to INT
);
