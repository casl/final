Thi demo has details on 
how to create visualizations using the data mart graphs in banking analytics with Docker and MetaBase.

1) Docker URL : https://hub.docker.com/r/finlibre/banking_analytics_stack/

2) YouTube link to start Docker : https://www.youtube.com/watch?v=74tt9pxWGeQ

3) YouTube link to use MetaBase : https://www.youtube.com/watch?v=ygRW2ziquX0
