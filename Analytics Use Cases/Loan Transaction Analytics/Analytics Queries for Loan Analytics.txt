Question : Number of Loan Transactions
SQL : SELECT count(*) AS "count"
FROM "public"."loan_transactions"

Question : Number of Loans Transferred Over time
SQL : SELECT date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)) AS "loan_transferred_date", count(*) AS "count"
FROM "public"."loan_transactions"
GROUP BY date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp))
ORDER BY date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)) ASC


Question : Number of Call Log Vs the number of Loans Transferred
SQL SELECT date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)) AS "loan_transferred_date", sum("public"."loan_transactions"."noofcalllog") AS "sum", avg("public"."loan_transactions"."noofcalllog") AS "avg"
FROM "public"."loan_transactions"
GROUP BY date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp))
ORDER BY date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)) ASC

Question: Transactions per day of the week
SQL : SELECT (CAST(extract(dow from CAST("public"."loan_transactions"."last_payment_date" AS timestamp)) AS integer) + 1) AS "last_payment_date", count(*) AS "count"
FROM "public"."loan_transactions"
GROUP BY (CAST(extract(dow from CAST("public"."loan_transactions"."last_payment_date" AS timestamp)) AS integer) + 1)
ORDER BY (CAST(extract(dow from CAST("public"."loan_transactions"."last_payment_date" AS timestamp)) AS integer) + 1) ASC

Question : Transactions per Month of the week
SQL : SELECT CAST(extract(month from CAST("public"."loan_transactions"."last_payment_date" AS timestamp)) AS integer) AS "last_payment_date", count(*) AS "count"
FROM "public"."loan_transactions"
GROUP BY CAST(extract(month from CAST("public"."loan_transactions"."last_payment_date" AS timestamp)) AS integer)
ORDER BY CAST(extract(month from CAST("public"."loan_transactions"."last_payment_date" AS timestamp)) AS integer) ASC

Question : Transactions Per Quarter
SQL : SELECT CAST(extract(quarter from CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)) AS integer) AS "loan_transferred_date", count(*) AS "count"
FROM "public"."loan_transactions"
GROUP BY CAST(extract(quarter from CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)) AS integer)
ORDER BY CAST(extract(quarter from CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)) AS integer) ASC


Question : Loans Transferred based on House Type
SELECT date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)) AS "loan_transferred_date", "public"."loan_transactions"."house_type_id" AS "house_type_id", count(*) AS "count"
FROM "public"."loan_transactions"
GROUP BY date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)), "public"."loan_transactions"."house_type_id"
ORDER BY date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)) ASC, "public"."loan_transactions"."house_type_id" ASC


Question : Loans Based on Referrel Status
SQL : SELECT date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)) AS "loan_transferred_date", "public"."loan_transactions"."referral_status" AS "referral_status", count(*) AS "count"
FROM "public"."loan_transactions"
GROUP BY date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)), "public"."loan_transactions"."referral_status"
ORDER BY date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)) ASC, "public"."loan_transactions"."referral_status" ASC

Question : Loans based on Designation 
SQL : SELECT date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)) AS "loan_transferred_date", "public"."loan_transactions"."designation_id" AS "designation_id", count(*) AS "count"
FROM "public"."loan_transactions"
GROUP BY date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)), "public"."loan_transactions"."designation_id"
ORDER BY date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)) ASC, "public"."loan_transactions"."designation_id" ASC

Question : Loans Based on Education Type
SQL : SELECT date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)) AS "loan_transferred_date", "public"."loan_transactions"."education_type_id" AS "education_type_id", count(*) AS "count"
FROM "public"."loan_transactions"
GROUP BY date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)), "public"."loan_transactions"."education_type_id"
ORDER BY date_trunc('month', CAST("public"."loan_transactions"."loan_transferred_date" AS timestamp)) ASC, "public"."loan_transactions"."education_type_id" ASC